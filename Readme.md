Tutorials Revisions - System
=================

This is a custom system developed by Gary to implement the functionality of submitting and using Revisions with Tutorials Application.

## Features

* Add Revision
* Discard Revision
* Delete Revision Permanently (Staff Only)
* Send Notification of Approval to Revision Author
* Increase Reputation of author on Revision Approval
* Compare Revision with Current State

<br >

Conflicts with Tutorials Application
==================

## Cannot Remove Attachments

Wrong table name is used in method `canRemove()` which we need to replace with the corrent name `tutorials_articles`. This will allow us to fix SQL 503 error causing on deleting the attachments in Tutorials Application.

<br >

**Target File :**

`admin\applications_addon\other\tutorials\extensions\attachments\plugin_tutorials.php`

**Find in `canRemove()` Method :**

```php
$tutorial = $this->DB->buildAndFetch( array( 'select' => '*', 'from' => 'tutorials', 'where' => 'a_id=' . $attachment['attach_rel_id'] ) );
```

**Replace with :**

```php
$tutorial = $this->DB->buildAndFetch( array( 'select' => '*', 'from' => 'tutorials_articles', 'where' => 'a_id=' . $attachment['attach_rel_id'] ) );
```

<br>

## Tutorial Display Revision Integration

This part allows us to integration Revisions into Tutorial Display Module (Public)

<br>

**Modulfe File :**

`admin/applications_addon/other/tutorials/modules_public/display/article.php`

**Find :**

``` php
		/* Load the author data */
		$author = IPSMember::buildDisplayData( $a['a_mid'] ? $a['a_mid'] : IPSMember::setUpGuest() );

		/* Like class */
		require_once( IPS_ROOT_PATH . 'sources/classes/like/composite.php' );
		$_like = classes_like::bootstrap( 'tutorials', 'articles' );

		/* Comments class */
		require_once( IPS_ROOT_PATH . 'sources/classes/comments/bootstrap.php' );
		$_comments = classes_comments_bootstrap::controller( 'tutorials-articles' );
```

**Add Below :**

```php
		// -----------------------------------
		// Tutorial Revision Integration
		// -----------------------------------
		if ($this->settings['tutrevisions_enable'])
		{
			// Load Tutorial Revision Library
			$classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir('tutrevisions') .'/sources/classes/TutRevisions.php', 'TutRevisions');
			$this->registry->setClass('TutRevisions', new $classToLoad( $this->registry ) );

			// Article Revision
			$a['revision'] = $this->registry->TutRevisions->getTutorialRevisionData($a);

			// Check if Revision is approved and post exists
			if ($a['revision']['r_approved'] == '1' && $a['revision']['r_post'])
			{
				// Replace Article Content with Revision Post
				$a['a_content'] = $a['revision']['r_post'];
			}
		}
		// -----------------------------------
		// End : Tutorial Revision Integration
		// -----------------------------------
```








