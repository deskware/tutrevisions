<?php

/**
* Copyright (C) 2018 Gary - All Rights Reserved
*
* Notice : All informations contained here is, and remains property of the Developer.
* You shall not share, modify or distribute this code without having permission from the Developer.
*
*/

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>You mad bro?</h1><h4>You cannot access this file directly</h4>";
	exit();
}

class app_class_tutrevisions
{
	protected $registry;
	protected $DB;
	protected $settings;
	protected $request;
	protected $lang;
	protected $member;
	protected $memberData;
	protected $cache;
    protected $caches;

	public function __construct (ipsRegistry $registry)
	{
		// Make objects
		$this->registry		= $registry;
		$this->DB			= $this->registry->DB();
		$this->settings		=& $this->registry->fetchSettings();
		$this->request		=& $this->registry->fetchRequest();
		$this->lang			= $this->registry->getClass('class_localization');
		$this->member		= $this->registry->member();
		$this->memberData	=& $this->registry->member()->fetchMemberData();
		$this->cache		= $this->registry->cache();
		$this->caches		=& $this->registry->cache()->fetchCaches();

		// Load Library
		$classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir('tutrevisions') .'/sources/classes/TutRevisions.php', 'TutRevisions');
		$this->registry->setClass('TutRevisions', new $classToLoad( $this->registry ) );
	}
}
