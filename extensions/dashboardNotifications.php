<?php

class dashboardNotifications__tutrevisions
{
    protected $registry;
    protected $DB;
    protected $settings;

    public function __construct()
    {
        $this->registry     = ipsRegistry::instance();
        $this->DB			=  $this->registry->DB();
        $this->settings		=& $this->registry->fetchSettings();
    }

    public function get()
    {
        $warnings = array();

        if ($this->settings['tutrevisions_acpwarning_enable'])
        {
            // Load TutRevision Class Library
            $TutRevisions = IPSLib::loadLibrary( IPSLib::getAppDir('tutrevisions') .'/sources/classes/TutRevisions.php', 'TutRevisions');

            // Fetch unapproved revisions
            $revisions = $this->DB->buildAndFetch(array('select' => 'COUNT(*) as total', 'from' => $TutRevisions::TUTORIAL_REVISIONS_TABLE, 'where' => 'r_approved=0'));

            // Check if we got any unapproved revisions
            if ((int) $revisions['total'] > 0)
            {
                $s      = $revisions['total'] > 1 ? "s" : "";
                $is_are = $revisions['total'] > 1 ? "are" : "is";
                $url    = $this->settings['base_url'] . "app=tutrevisions&module=revisions&section=manage&status=pending";

                // Set warning into ACP dashboard
                $warnings[] = array("{$revisions['total']} Unpproved Tutorial Revision$s $is_are found", "Please take required actions for the pending revisions of tutorials <a target='_blank' href='$url'>here</a>" );
            }
        }

        return $warnings;
    }
}
