<?php

/**
* Copyright (C) 2018 Gary - All Rights Reserved
*
* Notice : All informations contained here is, and remains property of the Developer.
* You shall not share, modify or distribute this code without having permission from the Developer.
*
*/

class tutrevisions_notifications
{
	public function getConfiguration()
	{
		ipsRegistry::instance()->getClass('class_localization')->loadLanguageFile( array( 'public_global' ), 'tutrevisions' );

		// Tutorial Request Notification Settings
		$_NOTIFY = array(

		array('key' => 'tutrevision_approved', 'default' => array('inline'), 'disabled' => array('email', 'mobile') ),

		);

		return $_NOTIFY;
	}
}