<?php

/**
* Copyright (C) 2018 Gary - All Rights Reserved
*
* Notice : All informations contained here is, and remains property of the Developer.
* You shall not share, modify or distribute this code without having permission from the Developer.
*
*/

class admin_tutrevisions_revisions_manage extends ipsCommand
{
    protected $tutorialData;
    protected $revisionData;
    protected $tabFilters = array(
        'all'       => 'All',
        'pending'   => 'Pending',
        'approved'  => 'Approved',
        'discarded' => 'Discarded'
    );

    public function doExecute (ipsRegistry $registry)
    {
		//-----------------------------------------
		// Load skin
		//-----------------------------------------
		$this->template     = $this->registry->output->loadTemplate('cp_skin_tutrevisions');
		$this->form_code    = $this->template->form_code = 'module=revisions&amp;section=manage';
        $this->form_code_js = $this->template->form_code_js = 'module=revisions&section=manage';

        // Lets initialize things
        $this->initialize();

        // Do something
        switch ($this->request['do'])
        {
            case 'compare':
                $this->showComparison();
            break;
            case 'archive':
                $this->showRevisionsArchive();
            break;
            default :
                $this->showManager();
            break;
        }

        // Do Submit
        switch ($this->request['doSubmit'])
        {
            case 'approve':
                $this->changeRevisionStatus(1);
            break;
            case 'discard':
                $this->changeRevisionStatus(2);
            break;
            case 'delete':
                $this->showDeleteRevision();
            break;
        }

        // Send Output
		$this->registry->getClass('output')->html_main .= $this->registry->getClass('output')->global_template->global_frame_wrapper();
		$this->registry->getClass('output')->sendOutput();
    }

    private function initialize ()
    {
        // Check if tutorial id is available in request
        if (is_numeric($this->request['tutorial']))
        {
            // Set Tutorial Data
            $this->tutorialData = $this->registry->TutRevisions->getTutorial((int) $this->request['tutorial']);
        }

        // Check if revision id is available in request
        if (is_numeric($this->request['revision']))
        {
            // Set Revision Data
            $this->revisionData = $this->registry->TutRevisions->getRevision((int) $this->request['revision']);

            // Check revision exists and no tutorial is requested
            if ($this->revisionData['r_tutorial_id'] && !$this->tutorialData && !$this->request['tutorial'])
            {
                // Set Tutorial Data
                $this->tutorialData = $this->registry->TutRevisions->getTutorial((int) $this->revisionData['r_tutorial_id']);
            }
        }
    }

    public function showManager ()
    {
        // Check if requested pending revisions
        if ($this->request['status'] == 'pending')
            $where = " r.r_approved=0 ";
        // Check if requested approved revisions
        else if ($this->request['status'] == 'approved')
            $where = " r.r_approved=1 ";
        // Check if requested discarded revisions
        else if ($this->request['status'] == 'discarded')
            $where = " r.r_approved=2 ";
        else
            $where = "";

        // Total Counts
        $count = $this->DB->buildAndFetch(array(
            'select'    => 'COUNT(*) as total',
            'from'      => array(TutRevisions::TUTORIAL_REVISIONS_TABLE => 'r'),
            'where'     => $where
        ));

		// Generate pagination
		$perpage	= $this->settings['tutrevisions_perpage'];
		$st			= intval($this->request['st']);
		$pages		= $this->registry->output->generatePagination( array(
			'totalItems'		=> intval($count['total']),
			'itemsPerPage'		=> $perpage,
			'currentStartValue'	=> $st,
			'baseUrl'			=> $this->settings['base_url'] . $this->form_code . "&amp;status=" . $this->request['status'],
		));


        // Get Revisions based on Status request etc..
        $this->DB->build( array(
            'select'    => 'r.*',
            'from'      => array(TutRevisions::TUTORIAL_REVISIONS_TABLE => 'r'),
            'where'     => $where,
            'add_join'  => array(
                array(
                    'select'    => 'm.*',
                    'from'      => array('members' => 'm'),
                    'where'     => 'r.r_member_id=m.member_id',
                    'type'      => 'left'
                ),
                array(
                    'select'    => 't.*',
                    'from'      => array(TutRevisions::TUTORIAL_ARTICLES_TABLE => 't'),
                    'where'     => 't.a_id=r.r_tutorial_id',
                    'type'      => 'left'
                )
            ),
            'order'     => 'r.r_id DESC',
            'limit'     => array($st, $perpage)
        ));

        $fire = $this->DB->execute();

        while ($row = $this->DB->fetch($fire))
        {
            $row['r_date_format'] = $this->registry->class_localization->getDate( $row['r_date'], 'SHORT' );
            $records[] = $row;
        }

        $options['tabFilters']  = $this->tabFilters;
        $options['pages']       = $pages ? $pages . "<br />" : null;

        $this->registry->output->html = $this->template->showManager( $records, $options );
    }


    public function showComparison ()
    {
		// Load TextDiff Library
		$classToLoad    = IPSLib::loadLibrary( IPSLib::getAppDir(TutRevisions::APPNAME) .'/sources/classes/TextDiff/src/TextDiff.php', 'TextDiff');
        $options        = array();

        // Post Contents
        $tutorialPost   = $this->tutorialData['a_content'];
        $revisionPost   = $this->revisionData['r_post'];

        // Clean up shit
        $tutorialPost   = strip_tags(str_replace("<p>&nbsp;</p>","", $tutorialPost));
        $revisionPost   = strip_tags(str_replace("<p>&nbsp;</p>","", $revisionPost));

        // Table Difference content
        $diff           = array(
            'render'           => TextDiff::render($tutorialPost, $revisionPost),
            'isEmpty'          => TextDiff::isEmpty(),
            'edited_count'     => TextDiff::countAddedLines(),
            'deleted_count'    => TextDiff::countDeletedLines() - TextDiff::countAddedLines()
        );

        // Assign Options
        $options['revision']    = $this->revisionData;
        $options['author']      = IPSMember::load( $this->revisionData['r_member_id'] );

        // Serve Output
        $this->registry->output->html = $this->template->showComparison($diff, $options);
    }

    public function changeRevisionStatus ($status)
    {
        // Default Tutorial Revision Status Codes
        $_codes = TutRevisions::STATUS_CODES;

        // Check if status code is correct
        if (!isset($_codes[$status])) $this->registry->output->showError("Invalid Status Request", __LINE__);

        // Check if revision not found
        if (!$this->revisionData) $this->registry->output->showError("Revision not found", __LINE__);

        // Create revision data
        $update = array(
            'r_approved'    => (int) $status,
            'r_mod_id'      => $this->memberData['member_id']
        );

        // Update exisiting revision
        if ($this->registry->TutRevisions->updateRevision($this->revisionData['r_id'], $update))
        {
            // Check if status is approved
            if ($status == 1)
            {
                // Tutorial Informations
                $tuturl     = $this->registry->output->buildSEOUrl( 'app=tutorials&article=' . $this->tutorialData['a_id'], 'public', $this->tutorialData['a_name_seo'], 'article' );
                $tutname    = $this->tutorialData['a_name'] ? $this->tutorialData['a_name'] : $tuturl;

                // Create notification options
                $n_options = array(
                    'notify_title'  => $this->settings['tutrevisions_notify_approve_title'],
                    'notify_text'   => sprintf(
                        $this->settings['tutrevisions_notify_approve_text'],
                        "[url={$tuturl}]{$tutname}[/url]"
                    )
                );

                // Apply Revision on Tutorial
                $this->registry->TutRevisions->applyRevisionOnTutorial($this->revisionData);

                // Wanna send notification
                if ($this->request['revision_approval_notify'] == 1)
                    $this->registry->TutRevisions->sendApprovalNotification($this->revisionData['r_member_id'], $n_options);

                // Wanna reputation up the author
                if ($this->request['revision_repup_author'] == 1)
                    $this->registry->TutRevisions->repUpAuthor($this->revisionData['r_member_id'], $this->revisionData['r_id']);
            }

            // Redirect back to manage revisions
            $this->registry->output->redirectScreen( "Revision " . $_codes[$status]['name'] . " Sucessfully", $this->settings['base_url'] . $this->form_code . "&do=compare&revision=". $this->revisionData['r_id']);
        }
    }

    public function showDeleteRevision ()
    {
        // Check if revision not found
        if (!$this->revisionData) $this->registry->output->showError("Revision not found", __LINE__);

        // Delete Revision
        $this->registry->TutRevisions->deleteRevision($this->revisionData['r_id']);

        // Redirect back to manage revisions
        $this->registry->output->redirectScreen( "Revision Deleted Permanentely", $this->settings['base_url'] . $this->form_code);
    }

    public function showRevisionsArchive ()
    {
        // Check if we have valid tutorial id
        if (!is_numeric($this->request['tutorial'])) $this->registry->output->showError("Tutorial not found", __LINE__);

        // Get Revisions based on Status request etc..
        $this->DB->build( array(
            'select'    => 'r.*',
            'from'      => array(TutRevisions::TUTORIAL_REVISIONS_TABLE => 'r'),
            'where'     => 'r.r_tutorial_id=' . (int) $this->request['tutorial'],
            'add_join'  => array(
                array(
                    'select'    => 'm.*',
                    'from'      => array('members' => 'm'),
                    'where'     => 'r.r_member_id=m.member_id',
                    'type'      => 'left'
                ),
                array(
                    'select'    => 't.*',
                    'from'      => array(TutRevisions::TUTORIAL_ARTICLES_TABLE => 't'),
                    'where'     => 't.a_id=r.r_tutorial_id',
                    'type'      => 'left'
                )
            ),
            'order'     => 'r.r_id DESC',
        ));

        $fire = $this->DB->execute();

        while ($row = $this->DB->fetch($fire))
        {
            $row['r_date_format'] = $this->registry->class_localization->getDate( $row['r_date'], 'SHORT' );
            $revisions[] = $row;
        }

        // Serve Output
        $this->registry->output->html = $this->template->showArchive($this->tutorialData, $revisions);
    }
}