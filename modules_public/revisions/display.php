<?php

/**
* Copyright (C) 2018 Gary - All Rights Reserved
*
* Notice : All informations contained here is, and remains property of the Developer.
* You shall not share, modify or distribute this code without having permission from the Developer.
*
*/

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>You mad bro?</h1><h4>You cannot access this file directly</h4>";
	exit();
}

class public_tutrevisions_revisions_display extends ipsCommand
{
    protected $tutorialData;
    protected $page;
    protected $showing = false;

    public function doExecute (ipsRegistry $registry)
    {
        // Lets Bootstrap stuff
        $this->bootstrapStuff();

        // Lets do something
        switch ($this->request['do'])
        {
            case 'contributors':
                $this->showContributors();
            break;
        }

        // Check if we are not showing, throw error
        if (! $this->showing) $this->registry->output->showError("Seems like you're trying to access a void", __LINE__);

        // Print Output
		$this->registry->output->setTitle( $this->page['title'] . ' - ' . $this->settings['board_name']);
		$this->registry->output->addContent( $this->output );
		$this->registry->output->sendOutput();
    }

    private function bootstrapStuff ()
    {
        // Load the TutRevision library if not loaded
        if (! $this->registry->isClassLoaded('TutRevisions'))
        {
            $classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir('tutrevisions') .'/sources/classes/TutRevisions.php', 'TutRevisions');
            $this->registry->setClass('TutRevisions', new $classToLoad( $this->registry ) );
        }

        // Check permission
        $this->permissionCheck();

        // Check if we haven't got tutorial refrence : Show Error Page
        if (! is_numeric($this->request['tutorial'])) $this->registry->output->showError("Invalid Request without refrence", __LINE__);

        // Set Tutorial Data based on refrence id in request
        $this->tutorialData = $this->registry->TutRevisions->getTutorial( (int) $this->request['tutorial']);
    }

    private function permissionCheck ()
    {
        // Check if system is not enabled
        if (! $this->settings['tutrevisions_enable']) $this->registry->output->showError("System is Offline", __LINE__);
    }

    private function showContributors ()
    {
        $data                   = array();
        $contributors           = array();
        $this->showing          = true;
        $this->page['title']    = "Tutorial Contributors";

        // Fetch Tutorial Revisions Contributors
        $this->DB->build(array(
            'select'    => 'r.r_member_id, r.r_tutorial_id, COUNT(*) as repeats',
            'from'      => array(TutRevisions::TUTORIAL_REVISIONS_TABLE => 'r'),
            'where'     => 'r.r_approved=1 AND r.r_tutorial_id=' . (int) $this->tutorialData['a_id'],
            'group'     => 'r.r_member_id',
            'add_join'  => array(
                array(
                    'select'    => 'm.member_id, m.members_display_name, m.members_seo_name, m.member_group_id',
                    'from'      => array('members' => 'm'),
                    'where'     => 'r.r_member_id=m.member_id',
                    'type'      => 'left'
                ),
                array(
                    'select'    => 'p.*',
                    'from'      => array('profile_portal' => 'p'),
                    'where'     => 'r.r_member_id=p.pp_member_id',
                    'type'      => 'left'
                ),
            ),
            'order'     => 'repeats DESC'
        ));

        $fire = $this->DB->execute();

        while ($row = $this->DB->fetch($fire))
        {
            if (! isset($contributors[$row['member_id']]))
            {
                $contributors[$row['member_id']] = $row;
            }
        }

        // Set Template Data
        $data['tutorialData']   = $this->tutorialData;
        $data['contributors']   = $contributors;

        // Serve Output
        $this->registry->output->addNavigation($this->tutorialData['a_name'],  'app=tutorials&article=' . $this->tutorialData['a_id'], $this->tutorialData['a_name_seo'], 'article' );
        $this->output = $this->registry->output->getTemplate('tutrevisions')->contributorsList($data);
    }
}