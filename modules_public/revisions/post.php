<?php

/**
* Copyright (C) 2018 Gary - All Rights Reserved
*
* Notice : All informations contained here is, and remains property of the Developer.
* You shall not share, modify or distribute this code without having permission from the Developer.
*
*/

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>You mad bro?</h1><h4>You cannot access this file directly</h4>";
	exit();
}

class public_tutrevisions_revisions_post extends ipsCommand
{
    protected $tutorialData = array();
    protected $revisionData = array();
    protected $page = array();
    protected $output;
    protected $parser;

    public function doExecute (ipsRegistry $registry)
    {
        // Lets Bootstrap stuff
        $this->bootstrapStuff();

        // Do something
        switch ($this->request['do'])
        {
            case 'submit':
                $this->saveRevision();
            break;
            case 'preview':
                $this->previewRevision();
            break;
            case 'edit':
                $this->showEditRevision();
            break;
            default:
                $this->showAddRevision();
        }

        // Print Output
		$this->registry->output->setTitle( $this->page['title'] . ' - ' . $this->settings['board_name']);
		$this->registry->output->addContent( $this->output );
		$this->registry->output->sendOutput();
    }

    private function bootstrapStuff ()
    {
        // Load the TutRevision library if not loaded
        if (! $this->registry->isClassLoaded('TutRevisions'))
        {
            $classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir('tutrevisions') .'/sources/classes/TutRevisions.php', 'TutRevisions');
            $this->registry->setClass('TutRevisions', new $classToLoad( $this->registry ) );
        }

        // Check permission
        $this->permissionCheck();

		/* Language */
        $this->lang->loadLanguageFile( array( 'public_post' ), 'forums' );

        // Check if we haven't got tutorial refrence : Show Error Page
        if (! is_numeric($this->request['tutorial'])) $this->registry->output->showError("Invalid Request without refrence", __LINE__);

        // Set Tutorial Data based on refrence id in request
        $this->tutorialData = $this->registry->TutRevisions->getTutorial( (int) $this->request['tutorial']);

        // Check if we got revision id in request
        if (is_numeric($this->request['revision']))
        {
            // Get revision data and set to Property
            $this->revisionData = $this->registry->TutRevisions->getRevision((int) $this->request['revision']);
        }
    }


    private function permissionCheck ()
    {
        // Check if system is not enabled
        if (! $this->settings['tutrevisions_enable'])
            $this->registry->output->showError("System is Offline", __LINE__);

        $canAdd     = in_array($this->memberData['member_group_id'], explode(",", $this->settings['tutrevisions_canadd_groups']));
        $isStaff    = in_array($this->memberData['member_group_id'], explode(",", $this->settings['tutrevisions_staff_groups']));

        // Check if we cannot add revision post, and if we are not even a staff
        if (!$canAdd && !$isStaff)
        {
            $this->registry->output->showError("Permission Denied", __LINE__);
        }

        // Check if we are allowed to edit
        if ($this->request['do'] == 'edit' && !$isStaff)
        {
            $this->registry->output->showError("Permission Prohibited", __LINE__);
        }

        // Check if Revision is already submitted for user (and is pending)
        if (!$isStaff && count($this->registry->TutRevisions->getRevisionByMember($this->memberData['member_id'], 0)))
        {
            $this->registry->output->showError("Previously submitted Revision is still pending for approval, please wait till our staff resolves it for you", __LINE__);
        }
    }

    public function showAddRevision ($type="add")
    {
        // Template Data
        $data = array();

        // Tutorial content for editor before saving
        if ($type == "add")
        {
            # Bug : Content maybe not required to be presave parsed for showing into editor
            # $content = $this->registry->TutRevisions->preSaveContent( $this->tutorialData['a_content'] );
            $content = $this->tutorialData['a_content'];
        }
        else if ($type == "edit")
        {
            $data['revision']   = $this->revisionData;
            # Bug : Content maybe not required to be presave parsed for showing into editor
            # $content            = $this->registry->TutRevisions->preSaveContent( $this->revisionData['r_post'] );
            $content            = $this->revisionData['r_post'];
        }

        // Set Data for Template
        $data['tutorial']   = $this->tutorialData;
        $data['editor']     = $this->registry->TutRevisions->loadEditor('tutorial_revision',null , $content );

        // Grab render attach class
        $classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir( 'core' ) . '/sources/classes/attach/class_attach.php', 'class_attach', 'core' );
        $class_attach = new $classToLoad( $this->registry );

        // Initialize Class Attachments
        $class_attach->type     = 'tutorials';
        $class_attach->post_key = $this->tutorialData['a_post_key'];
        $class_attach->init();
        $class_attach->getUploadFormSettings();

        // Upload Form
        $data['uploadForm'] = $this->registry->output->getTemplate('post')->uploadForm(
            $this->tutorialData['a_post_key'],
            'tutorials',
            $class_attach->attach_stats,
            $this->tutorialData['a_id'],
            $this->tutorialData['a_cat']
        );

		// Page output things
        $this->page['title'] = $type == 'edit' ? "Edit Tutorial Revision" : "Tutorial Revision";

        // Additional Data
        $data['heading'] = $this->page['title'];
        $data['formtype'] = $type;

        // Set output
		$this->output = $this->registry->output->getTemplate('tutrevisions')->revisionForm($data);
    }

    public function saveRevision ()
    {
        // Check if we are using correct request method
        if ($this->request['request_method'] != "post" || $this->request['secure_key'] != $this->member->form_hash)
            $this->registry->output->showError("Bad Request", __LINE__);


        // Check if revision is not exist [We are adding new revision]
        if (!$this->revisionData['r_id'])
        {
            // Create revision data
            $data = array(
                'r_tutorial_id' => $this->tutorialData['a_id'],
                'r_member_id'   => $this->memberData['member_id'],
                'r_approved'    => 0,
                'r_date'        => time(),
                'r_post'        => $this->registry->TutRevisions->preSaveContent( $_POST['tutorial_revision'] )
            );

            // Add new revision
            if ($this->registry->TutRevisions->addRevision($data))
            {
                // Redirect back to Tutorial
                $this->registry->output->redirectScreen( "Revision Added Sucessfully", $this->settings['base_url'] . "app=tutorials&article={$this->tutorialData['a_id']}", $this->tutorialData['a_name_seo'], 'article' );
            }
        }
        // We are updating already existed Revision
        else
        {
            // Create revision data
            $update = array(
                'r_edited'  => time(),
                'r_post'    => $this->registry->TutRevisions->preSaveContent( $_POST['tutorial_revision'] )
            );

            // Update exisiting revision
            if ($this->registry->TutRevisions->updateRevision($this->revisionData['r_id'], $update))
            {
                // Redirect back to Tutorial
                $this->registry->output->redirectScreen( "Revision Modified Sucessfully", $this->settings['base_url'] . "app=tutorials&article={$this->tutorialData['a_id']}", $this->tutorialData['a_name_seo'], 'article' );
            }
        }
    }

    public function previewRevision ()
    {
        // Template Data
        $data = array();

		// Grab the parser library
		$classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/text/parser.php', 'classes_text_parser' );
        $this->parser = new $classToLoad();

        // Set parser options
        $this->parser->set( array(
            'memberData'     => $this->memberData,
            'parseBBCode'    => 1,
            'parseArea'      => 'tutorial_revision_preview',
            'parseHtml'      => 0,
            'parseEmoticons' => 1
        ) );

        // Parse post content to display
        $postContent = $this->parser->display( $_POST['tutorial_revision'] );

        // Grab render attach class
        $classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir( 'core' ) . '/sources/classes/attach/class_attach.php', 'class_attach', 'core' );
        $class_attach = new $classToLoad( $this->registry );

        // Initialize Class Attachments
        $class_attach->type     = 'tutorials';
        $class_attach->post_key = $this->tutorialData['a_post_key'];
        $class_attach->init();
        $class_attach->getUploadFormSettings();

        // Upload Form
        $data['uploadForm'] = $this->registry->output->getTemplate('post')->uploadForm(
            $this->tutorialData['a_post_key'],
            'tutorials',
            $class_attach->attach_stats,
            $this->tutorialData['a_id'],
            $this->tutorialData['a_cat']
        );

        // Render Attachments and create Content for preview
        $attachData = $class_attach->renderAttachments( $postContent, array( $this->tutorialData['a_id'] ) );
        $preview = $attachData[0]['html'] . $attachData[0]['attachmentHtml'];

        // Tutorial content for editor before saving
        $content = $this->registry->TutRevisions->preSaveContent( $_POST['tutorial_revision'] );

        // Set Data for Template
        $data['tutorial']   = $this->tutorialData;
        $data['editor']     = $this->registry->TutRevisions->loadEditor('tutorial_revision',null , $content );
        $data['preview']    = $preview;

		// Page output things
        $this->page['title'] = $this->request['formtype'] == 'edit' ? "Edit Tutorial Revision" : "Tutorial Revision";

        // Additional Data
        $data['heading'] = $this->page['title'];
        $data['formtype'] = $this->request['formtype'];
        $data['revision'] = count($this->revisionData) ? $this->revisionData : null;

        // Set output
		$this->output = $this->registry->output->getTemplate('tutrevisions')->revisionForm($data);
    }


    public function showEditRevision ()
    {
        $this->showAddRevision("edit");
    }
}