<?php

$TABLE[] = "CREATE TABLE tutrevisions_revisions (
	r_id			int(10) NOT NULL auto_increment,
    r_tutorial_id   int(10) NOT NULL default 0,
	r_member_id	    int(10) NOT NULL default 0,
    r_mod_id        int(10) NOT NULL default 0,
    r_approved      int(10) NOT NULL default 0,
    r_active        int(1)  NOT NULL default 0,
	r_date			int(10) default NULL,
	r_edited		int(10) default NULL,
	r_post			mediumtext,
    r_original      mediumtext,

	PRIMARY KEY		(r_id),
	KEY				r_member_id (r_member_id),
	KEY				r_tutorial_id (r_tutorial_id)
);";

?>