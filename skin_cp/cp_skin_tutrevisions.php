<?php

class cp_skin_tutrevisions extends output
{
    public function showManager ($revisions, $options)
    {

		foreach ($options['tabFilters'] as $key => $name)
		{
            $active     = ($this->request['status'] == $key) || (!$this->request['status'] && $key == 'all') ? 'active' : '';
            $statusUri  = $key != 'all' ? "&status=" . $key : "";

            $TABS .= <<<HTML

<li class="ipsActionButton {$active}" data-status="{$key}">
	<a href="{$this->settings['base_url']}{$this->form_code}{$statusUri}">{$name}</a>
</li>

HTML;
        }



            $IPBHTML = <<<HTML

<!-- ============== STYLESHEET ================= -->
<style>
.ipsTable > tbody > tr:nth-child(odd) td {background: #fff;}
.ipsTable > tbody > tr:nth-child(even) td {background: #f5f8fa;}
.pagination {padding-left:0;}
.pagination .pagejump {display: none;}

#revisions_search_bar, .ipsSortBar {background: #849cb7;border-bottom: 1px solid #6984a1;font: normal 13px 'Helvetica Neue', helvetica, arial, sans-serif;padding: 8px;}
#revisions_results tr.active {border: 1px solid #9cceff;border-width: 1px 0 1px 0;}
#revisions_results tr.active > td {background: #e1f0ff;}
</style>


<div class="section_title">
	<h2>Manage Revisions</h2>
</div>

<br />

{$options['pages']}


<div class='acp-box'>
	<h3>Revisions</h3>
	<div id="revisions_search_bar" class="clearfix">
		<div class="ipsToggleBar left" id="revision_types" style="overflow: hidden">
			<ul>
			{$TABS}
			</ul>
		</div>
	</div>

	{$this->revisionsTable( $revisions )}
</div>

HTML;

        return $IPBHTML;
    }


    public function showComparison ($TextDiff, $options=array())
    {
        // Yesno Fields
        $approvalNotify_field   = $this->registry->output->formYesNo('revision_approval_notify', '1', 'revision_approval_notify');
        $repupAuthor_field      = $this->registry->output->formYesNo('revision_repup_author', '1', 'revision_repup_author');

        // Status Badge
        $_codes = TutRevisions::STATUS_CODES;
        $statusBadge = "<span class='ipsBadge badge_" . $_codes[$options['revision']['r_approved']]['color'] . "'>" . $_codes[$options['revision']['r_approved']]['name'] . "</span>";

        // Author Information
        $author_url = $this->registry->output->buildSEOUrl("showuser={$options['author']['member_id']}", 'public', $options['author']['members_seo_name'], 'showuser' );

        // Check if revision is not approved
        if ($options['revision']['r_approved'] != '1')
        {
            // Approve Button
            $approveButton = <<<HTML
<button type="submit" name="doSubmit" value="approve" class="button primary">Approve Revision</button>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
HTML;

            // Check if revision is not discarded
            if ($options['revision']['r_approved'] != '2')
            {
                // Discard Button
                $discardButton = <<<HTML
<button type="submit" name="doSubmit" value="discard" class="button redbutton">Discard Revision</button>
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
HTML;
            }
        }

        // Show Delete button if revision not unapproved
        if ($options['revision']['r_approved'] > 0)
        {
            $delmsg = "Are you sure you want to Permanently Delete this Revision from database? (This action cannot be undone)";
            $deleteButton = <<<HTML
<button type="submit" name="doSubmit" value="delete" class="button redbutton" onclick="return confirm('{$delmsg}')">Delete Revision Permanently</button>
HTML;
        }

        // Difference Table
        $tableDifference = $TextDiff['render'] ? $TextDiff['render'] : "<div class='information-box'>The content of the Original Tutorial and this Revision is identical</div>";


        // HTML
        $IPBHTML = <<<HTML

<style>
table.diff-heading{width:100%}
table.diff-heading td{text-align:center;padding:10px;font-size:15px;font-weight:700;background:#e6f2ff}
table.diff tbody tr td:nth-child(2){width:4%}
table.diff{border-collapse:separate;border-spacing:2px;table-layout:fixed;width:100%;white-space:pre-wrap}
table.diff col.content{width:auto}
table.diff col.ltype{width:30px}
table.diff tr{background-color:transparent}
table.diff td,table.diff th{font-family:Consolas,Monaco,monospace;font-size:14px;line-height:1.618;padding:.5em;vertical-align:top;word-wrap:break-word}
table.diff td h1,table.diff td h2,table.diff td h3,table.diff td h4,table.diff td h5,table.diff td h6{margin:0}
table.diff .diff-addedline ins,table.diff .diff-deletedline del{text-decoration:none}
table.diff .diff-deletedline{background-color:#ffe9e9}
table.diff .diff-deletedline del{background-color:#faa}
table.diff .diff-addedline{background-color:#e9ffe9}
table.diff .diff-addedline ins{background-color:#afa}

#revisionForm .ipsBadge {padding:2px 5px;}
.diffbar .ipsBadge {font-size: 11px;}
</style>

<div class="section_title">
	<h2>Revision Comparison to Tutorial</h2>
</div>

<br />

<div class='acp-box'>
	<h3>
        Comparision
    </h3>
    <table class="diff-heading">
        <tbody><tr><td>Original (Tutorial)</td><td>Modified (Revision)</td></tr></tbody>
    </table>
	<div style="padding:10px;">
        {$tableDifference}
    </div>
</div>

<br /><br />

<form id="revisionForm" action="{$this->settings['base_url']}{$this->form_code}" method="post">
<div class='acp-box'>
	<h3>Revision Options</h3>

<table class="ipsTable double_pad">
		<tbody>
        <tr>
			<th colspan="2" class="head"><strong>Information</strong></th>
        </tr>
		<tr>
			<td class="field_title">
				<strong class="title">Revision Author</strong>
			</td>
			<td class="field_field">
                <a href="{$author_url}" target="_blank">{$options['author']['members_display_name']}</a>
			</td>
        </tr>
		<tr>
			<td class="field_title">
				<strong class="title">Revision Current Status</strong>
			</td>
			<td class="field_field">
                {$statusBadge}
			</td>
        </tr>
		<tr>
			<td class="field_title">
				<strong class="title">Changes made by Revision</strong>
			</td>
			<td class="field_field">
                <div class="diffbar">
                    Lines Edited &nbsp; <span class="ipsBadge badge_grey">{$TextDiff['edited_count']}</span> &nbsp; &nbsp;
                    Lines Removed &nbsp; <span class="ipsBadge badge_grey">{$TextDiff['deleted_count']}</span>
                </div>
			</td>
        </tr>
        <tr>
			<th colspan="2" class="head"><strong>Editing Settings</strong></th>
        </tr>
		<tr>
			<td class="field_title">
				<strong class="title">Want to edit this Revision?</strong>
			</td>
			<td class="field_field">
                <ul>
			        <li class="ipsActionButton">
				        <a target="_blank" href="{$this->settings['board_url']}/index.php?app=tutrevisions&module=revisions&section=post&do=edit&revision={$options['revision']['r_id']}&tutorial={$options['revision']['r_tutorial_id']}">Edit this Revision</a>
			        </li>
		        </ul>
			</td>
        </tr>
		<tr>
			<td class="field_title">
				<strong class="title">Want to edit AMP Version of Tutorial?</strong>
			</td>
			<td class="field_field">
                <ul>
			        <li class="ipsActionButton">
				        <a target="_blank" href="{$this->settings['board_url']}/index.php?app=ampcontent&module=post&section=post&do=edit&a_rel_type=tutorial&a_rel_id={$options['revision']['r_tutorial_id']}">Edit Tutorial AMP Version</a>
			        </li>
		        </ul>
			</td>
        </tr>
        <tr>
			<th colspan="2" class="head"><strong>Approval Settings</strong></th>
        </tr>
		<tr>
			<td class="field_title">
				<strong class="title">Send Approval Notification to Author?</strong>
			</td>
			<td class="field_field">
                {$approvalNotify_field}
			</td>
		</tr>
		<tr>
			<td class="field_title">
				<strong class="title">Reputation Up Author on Approval?</strong>
			</td>
			<td class="field_field">
                {$repupAuthor_field}
			</td>
		</tr>

    </tbody></table>

    <br />

    <div class="acp-actionbar">
	    {$approveButton}
        {$discardButton}
        {$deleteButton}
    </div>

    <fieldset>
        <input type="hidden" name="revision" value="{$options['revision']['r_id']}" />
    </fieldset>

</div>
</form>


HTML;

        return $IPBHTML;
    }


	public function revisionsTable ($revisions)
	{
		$ROWS_HTML = "";

		if (is_array($revisions) && count($revisions))
		{
			foreach ($revisions as $revision)
			{
				$ROWS_HTML .= $this->revisionTableRow($revision);
			}
        }

        $IPBHTML = <<<HTML

<table class="ipsTable double_pad" id="revisions_list">
<thead>
	<tr>
		<th style="width: 2%"></th>
		<th style="width: 35%">Tutorial Name</th>
		<th style="width: 10%">Author</th>
		<th style="width: 10%">Status</th>
		<th style="width: 15%">Submission Date</th>
		<th style="width:15%; text-align: right; padding-right: 15px"></th>
	</tr>
</thead>
<tbody id="revisions_results">
{$ROWS_HTML}
</tbody>
</table>

HTML;

		return $IPBHTML;
	}


    /**
     * Revision Table Row
     *
     * @param   array   $revision   Revision Data
     * @return  string              HTML
     */

	public function revisionTableRow (array $revision)
	{
        // Status Badge
        $_codes = TutRevisions::STATUS_CODES;
        $statusBadge = "<span class='ipsBadge badge_" . $_codes[$revision['r_approved']]['color'] . "'>" . $_codes[$revision['r_approved']]['name'] . "</span>";

        // Author Information
        $author_url = $this->registry->output->buildSEOUrl("showuser={$revision['member_id']}", 'public', $revision['members_seo_name'], 'showuser' );

        // Tutorial Information
        $tuturl     = $this->registry->output->buildSEOUrl('app=tutorials&article=' . $revision['a_id'], 'public', $revision['a_name_seo'], 'article');

        // Is Active?
        $active     = $revision['r_active'] == '1' ? "active" : "";

        // HTML
        $IPBHTML .= <<<HTML

<tr class="revision_row ipsControlRow {$active}" id="revision_{$revision['r_id']}">
	<td><a>#{$revision['r_id']}</a></td>
	<td class="tutorial_name"><a target="_blank" href="{$tuturl}" class="larger_text">{$revision['a_name']}</a></td>
	<td><a target="_blank" href="{$author_url}">{$revision['members_display_name']}</a></td>
	<td>{$statusBadge}</td>
	<td>{$revision['r_date_format']}</td>
	<td class="col_buttons">
		<ul class="ipsControlStrip">
            <li class="i_text">
				<a href="{$this->settings['base_url']}{$this->form_code}&do=compare&revision={$revision['r_id']}" title="Compare Revision"><img src="{$this->settings['skin_acp_url']}/images/icons/application_double.png"></a>
			</li>
            <li class="i_text archive">
				<a href="{$this->settings['base_url']}{$this->form_code}&do=archive&tutorial={$revision['r_tutorial_id']}" title="Show All Revisions Archive"><img src="{$this->settings['skin_acp_url']}/images/icons/archives.png"></a>
			</li>
		</ul>
	</td>
</tr>

HTML;
		return $IPBHTML;
    }

    public function showArchive (array $tutorial, array $revisions)
    {
        // HTML
        $IPBHTML = <<<HTML

<!-- ============== STYLESHEET ================= -->
<style>
.ipsTable > tbody > tr:nth-child(odd) td {background: #fff;}
.ipsTable > tbody > tr:nth-child(even) td {background: #f5f8fa;}
.pagination {padding-left:0;}
.pagination .pagejump {display: none;}
.ipsControlStrip li.archive {display:none;}
#revisions_results tr.active {border: 1px solid #9cceff;border-width: 1px 0 1px 0;}
#revisions_results tr.active > td {background: #e1f0ff;}
</style>

<div class="section_title">
	<h2>Revisions Archive : {$tutorial['a_name']}</h2>
</div>

<br />

<div class='acp-box'>
	<h3>All Revisions of this Tutorial</h3>
    {$this->revisionsTable( $revisions )}
</div>

<br /><br />
HTML;

        return $IPBHTML;
    }
}