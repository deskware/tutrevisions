<?php

/**
* Copyright (C) 2018 Gary - All Rights Reserved
*
* Notice : All informations contained here is, and remains property of the Developer.
* You shall not share, modify or distribute this code without having permission from the Developer.
*
*/

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>You mad bro?</h1><h4>You cannot access this file directly</h4>";
	exit();
}

class TutRevisions
{
	protected $registry;
	protected $DB;
	protected $settings;
	protected $request;
	protected $lang;
	protected $member;
	protected $memberData;
	protected $cache;
    protected $caches;

    const APPNAME = "tutrevisions";
    const TUTORIAL_ARTICLES_TABLE = "tutorials_articles";
    const TUTORIAL_REVISIONS_TABLE = "tutrevisions_revisions";
    const STATUS_CODES = array(
        0   => array('name'  => "Unapproved", 'color' => "grey"),
        1   => array('name'  => "Approved", 'color' => "green"),
        2   => array('name'  => "Discarded", 'color' => "red")
    );

	public function __construct (ipsRegistry $registry)
	{
		// Make registry objects
		$this->registry   =  $registry;
		$this->DB         =  $this->registry->DB();
		$this->settings   =& $this->registry->fetchSettings();
		$this->request    =& $this->registry->fetchRequest();
		$this->lang       =  $this->registry->getClass('class_localization');
		$this->member     =  $this->registry->member();
		$this->memberData =& $this->registry->member()->fetchMemberData();
		$this->cache      =  $this->registry->cache();
        $this->caches     =& $this->registry->cache()->fetchCaches();
    }

	public function getUrlACP ()
	{
		return $this->settings['base_acp_url'] . '/' . IPSLib::getAppFolder(self::APPNAME) . '/' . self::APPNAME;
    }

    public function getTutorial ($id)
    {
        $query = array(
            'select'    => '*',
            'from'      => self::TUTORIAL_ARTICLES_TABLE,
            'where'     => 'a_id=' . $id
        );

        return $this->DB->buildAndFetch( $query );
    }

    public function getRevision ($id)
    {
        $query = array(
            'select'    => '*',
            'from'      => self::TUTORIAL_REVISIONS_TABLE,
            'where'     => 'r_id=' . $id
        );

        return $this->DB->buildAndFetch( $query );
    }

    public function getRevisionByMember ($member_id, $approved=0)
    {
        $query = array(
            'select'    => '*',
            'from'      => self::TUTORIAL_REVISIONS_TABLE,
            'where'     => $this->whereCombine( array(
                'r_member_id=' . (int) $member_id,
                'r_approved=' . (int) $approved
            ))
        );

        return $this->DB->buildAndFetch( $query );
    }

	public function preSaveContent( $content, $section='' )
	{
		// Load Editor (Composite) Library
		$classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
		$editor = new $classToLoad();
		$content = $editor->process( $content );

		// Parse Post Content
		IPSText::getTextClass('bbcode')->parsing_section 	   = $section;
		IPSText::getTextClass('bbcode')->parse_smilies         = 1;
		IPSText::getTextClass('bbcode')->parse_html            = 0;
		IPSText::getTextClass('bbcode')->parse_nl2br           = 1;
		IPSText::getTextClass('bbcode')->parse_bbcode          = 1;
		IPSText::getTextClass('bbcode')->parsing_mgroup        = $this->memberData['member_group_id'];
		IPSText::getTextClass('bbcode')->parsing_mgroup_others = $this->memberData['mgroup_others'];

        // Return parsed content
		return IPSText::getTextClass('bbcode')->preDbParse( $content );
    }

	public function loadEditor( $name, $options=null, $content=null )
	{
        // Load IPS Editor Library
		$classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/editor/composite.php', 'classes_editor_composite' );
        $editor = new $classToLoad();

        // Set Editor Options
		$editor->setLegacyMode(0);
		$editor->setAllowHtml(0);

		return $content ? $editor->show( $name, $options, $content ) : $editor->show( $name, $options );
    }

    public function addRevision (array $data)
    {
        if (count($data))
        {
            $this->DB->insert(self::TUTORIAL_REVISIONS_TABLE, $data);
            return $this->DB->getInsertId();
        }
    }

    public function updateRevision ($id, array $data)
    {
		if (is_numeric($id) && is_array($data))
		{
            $this->DB->update(self::TUTORIAL_REVISIONS_TABLE, $data, "r_id={$id}");
            return true;
        }
    }

    public function deleteRevision ($id)
    {
        if (is_numeric($id))
        {
            $this->DB->delete(self::TUTORIAL_REVISIONS_TABLE, 'r_id='. $id);
        }
    }

    public function getTutorialRevisionData (array $article)
    {
        // Check if we haven't got revision id
        if (! is_numeric($article['revision_id'])) return;

        // Get and Assign Revision
        $revision = $this->getRevision($article['revision_id']);

        if ($revision['r_id'])
        {
            // Set if revision is pending
            $revision['isPending'] = $revision['r_member_id'] == $this->memberData['member_id'] && $r['r_approved'] == '0' ? true : false;
        }

        echo "HAHA";

        // Return Revision Data
        return $revision;
    }

    public function sendApprovalNotification ($member_id, $options=array())
    {
        if (!$member_id) return;

        // Load Notification Library
		$classToLoad		= IPSLib::loadLibrary( IPS_ROOT_PATH . '/sources/classes/member/notifications.php', 'notifications' );
        $notifyLibrary		= new $classToLoad( $this->registry );

        // Set Notification Properties
        $notifyLibrary->setMember( $member_id );
        $notifyLibrary->setFrom( $options['notify_sender_id'] ? $options['notify_sender_id'] : $this->memberData );
        $notifyLibrary->setNotificationKey('tutrevision_approved');
        $notifyLibrary->setNotificationText($options['notify_text']);
        $notifyLibrary->setNotificationTitle($options['notify_title']);

        try
        {
            // Try sending notification
            $notifyLibrary->sendNotification();
        }
        catch( Exception $e ){}
    }

    public function repUpAuthor ($member_id, $revision_id)
    {
        if (!$member_id || !$revision_id) return;

		// Get the rep library
		$classToLoad = IPSLib::loadLibrary( IPS_ROOT_PATH . 'sources/classes/class_reputation_cache.php', 'classReputationCache' );
        $repCache = new $classToLoad();

		// Add the rating
		return $repCache->addRate('tutorial_revision', $revision_id, 1, '', $member_id, self::APPNAME );
    }

    public function applyRevisionOnTutorial (array $revision)
    {
        // Check if required information is missing
        if (!$revision['r_post'] || !$revision['r_tutorial_id']) return;

        // Get Original Tutorial Content
        $tutorial = $this->getTutorial($revision['r_tutorial_id']);

        // Check if tutorial exists
        if ($tutorial['a_id'])
        {
            // Inactive currently active revision for this tutorial
            $this->DB->update(self::TUTORIAL_REVISIONS_TABLE, array('r_active' =>  0), "r_tutorial_id={$revision['r_tutorial_id']} AND r_active=1");

            // Save Original post to Revision and set as Active
            $this->updateRevision($revision['r_id'], array('r_original' => $tutorial['a_content'], 'r_active' => 1));

            // Update Tutorial Data
            $this->DB->update(self::TUTORIAL_ARTICLES_TABLE, array('a_content' => $revision['r_post']), 'a_id=' . $revision['r_tutorial_id']);
        }
    }

    public function pruneDiscardedRevisions ($days_old)
    {
        if ((int) $days_old > 0)
        {
            $where  = "r_approved=2 AND r_date < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL $days_old DAY))";
            $count  = $this->DB->buildAndFetch(array('select' => 'COUNT(*) as total', 'from' => self::TUTORIAL_REVISIONS_TABLE, 'where' => $where ));

            // Delete Records
            $this->DB->delete(self::TUTORIAL_REVISIONS_TABLE, $where);

            // Return Counts
            return $count['total'];
        }
    }

    private function whereCombine (array $where, $delimeter=" AND ")
    {
        if (count($where))
        {
            return implode($delimeter, $where);
        }
    }
}