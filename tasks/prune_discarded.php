<?php

/**
* Copyright (C) 2018 Gary - All Rights Reserved
*
* Notice : All informations contained here is, and remains property of the Developer.
* You shall not share, modify or distribute this code without having permission from the Developer.
*
*/

if ( ! defined( 'IN_IPB' ) )
{
	print "<h1>Incorrect access</h1>You cannot access this file directly. If you have recently upgraded, make sure you upgraded all the relevant files.";
	exit();
}

class task_item
{
	protected $class;
	protected $task = array();
    protected $registry;
    protected $settings;
    protected $lang;

	public function __construct ( ipsRegistry $registry, $class, $task )
	{
		$this->registry	= $registry;
		$this->lang		= $this->registry->getClass('class_localization');
        $this->settings =& $this->registry->fetchSettings();

		$this->class	= $class;
		$this->task		= $task;
    }

    public function runTask ()
    {
        $count = 0;
        $days_old = (int) $this->settings['tutrevisions_prune_daysold'];

        // Check if we have given X old days in Settings
        if ($days_old > 0)
        {
            // Load TutRevisions Library
            $classToLoad = IPSLib::loadLibrary( IPSLib::getAppDir('tutrevisions') .'/sources/classes/TutRevisions.php', 'TutRevisions');
            $TutRevisions = new $classToLoad( $this->registry );

            // Prune X Days old discarded Tutorial Revisions
            $count = (int) $TutRevisions->pruneDiscardedRevisions($days_old);

            //-----------------------------------------
            // Log to log table - modify but dont delete
            //-----------------------------------------

            $this->class->appendTaskLog( $this->task, "Pruned $count discarded Tutorial Revision(s)" );
        }

		//-----------------------------------------
		// Unlock Task: DO NOT MODIFY!
		//-----------------------------------------

		$this->class->unlockTask( $this->task );
    }
}